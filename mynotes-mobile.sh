#!/usr/bin/env bash
# -----------------------------------------------------------------------------
# Projeto   : mynotes-mobile
# Arquivo   : mynotes.sh
# Descrição :
# Versão    : 0.0.3.0
# Data      : 11/01/2023 - 20:12
# Autor     : Victor Araujo <victorbra@gmail.com>
# Licença   : GNU/GPL v3.0
# -----------------------------------------------------------------------------
# Uso: 
#   - F2 Exibe/oculta janela de pré visualização.
# Dependências: fzf, fd, bat
# Sugestões: Uma melhor experiência ocorrerá com a instalação dos seguintes plugins do vim: vimwiki e goyo.
# -----------------------------------------------------------------------------

# Variáveis do usuário --------------------------------------------------------
path_notes="/data/data/com.termux/files/home/storage/shared/Documents/notas"
template="
---
created: $(date +"%Y-%m-%d %H:%M")
tags: 
links: 
resources: "
editor="termux-open"

# Funções ---------------------------------------------------------------------
text_color (){
    echo "$(tput setaf ${2:-3})${1}$(tput sgr0)"
}

create_note (){
    clear
    text_color "NOVA ANOTAÇÃO" 3
    read -ep "Título: " titulo
    [[ -n "$titulo" ]] && lvim - +"w ${path_notes}/${titulo}.md" <<<"$template"
}

create_menu_notes (){
    local menu_base=":Nova anotação\n:Gerenciar anotações\n:Sair\n"
    local list_notes="$( fd --base-directory "${path_notes}" -e md -d 1)"
    menu_notes="${menu_base}${list_notes}"
    printf "$menu_notes"
}

default (){
    selected="$( create_menu_notes |
    fzf --layout=reverse --no-multi --cycle --prompt="Pesquisar: " \
 )"

    case "$selected" in
        :Nova*)
            create_note
            ;;
        :Gerenciar*)
            "$editor" "${path_notes}" &> /dev/null &
            ;;
          :Sair)
            exit
            ;;
        *)
            [[ -n "${selected}" ]] && "$editor" "${path_notes}/${selected#./*}"
            default
            ;;
    esac
}

# Execução --------------------------------------------------------------------
default
