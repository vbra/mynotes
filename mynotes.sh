#!/usr/bin/env bash
# -----------------------------------------------------------------------------
# Projeto   : mynotes
# Arquivo   : mynotes.sh
# Descrição :
# Versão    : 0.0.3.0
# Data      : 12/06/2021 - 14:16
# Autor     : Victor Araujo <victorbra@gmail.com>
# Licença   : GNU/GPL v3.0
# -----------------------------------------------------------------------------
# Uso: 
#   - F2 Exibe/oculta janela de pré visualização.
# Dependências: fzf, fd, bat
# Sugestões: Uma melhor experiência ocorrerá com a instalação dos seguintes plugins do vim: vimwiki e goyo.
# -----------------------------------------------------------------------------

# Variáveis do usuário --------------------------------------------------------
path_notes="/mnt/Arquivos/Nextcloud/VictorAraujo/wiki/pages"
template="
---
created: $(date +"%Y-%m-%d %H:%M")
tags: 
links: 
resources: "

# Funções ---------------------------------------------------------------------
text_color (){
    echo "$(tput setaf ${2:-3})${1}$(tput sgr0)"
}

create_note (){
    clear
    text_color "NOVA ANOTAÇÃO" 3
    read -ep "Título: " titulo
    [[ -n "$titulo" ]] && $HOME/.local/bin/nvim - +"w ${path_notes}/${titulo}.md" <<<"$template"
}

create_menu_notes (){
    local menu_base=":Nova anotação\n:Gerenciar anotações\n"
    local list_notes="$( fdfind --base-directory "${path_notes}" -e md -d 1)"
    menu_notes="${menu_base}${list_notes}"
    printf "$menu_notes"
}

default (){
    selected="$( create_menu_notes |
    fzf --layout=reverse --no-multi --cycle --prompt="Pesquisar: " \
    --preview-window=right:65%:wrap \
    --bind='F2:toggle-preview' \
    --preview="[[ -f "${path_notes}/{}" ]] && batcat --color always  --decorations never -l Markdown --theme gruvbox-dark ${path_notes}/{}" )"

    case "$selected" in
        :Nova*)
            create_note
            ;;
        :Gerenciar*)
            nemo "${path_notes}" &> /dev/null &
            ;;
        *)
            [[ -n "${selected}" ]] && $HOME/bin/glow -s dracula -p "${path_notes}/${selected#./*}" &&
                default
            ;;
    esac
}

# Execução --------------------------------------------------------------------
default
read
